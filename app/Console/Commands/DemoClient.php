<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use React\EventLoop\Factory as ReactFactory;
use React\Socket\Connector as SocketConnector;
use Ratchet\Client\Connector as ClientConnector;
use Ratchet\Client\WebSocket;
use Ratchet\RFC6455\Messaging\MessageInterface;
use Throwable;

class DemoClient extends Command
{
    protected $signature = 'demo:client';
    protected $description = 'demo web socket-client';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $uri = 'wss://stream.binance.com:9443/ws/bnbbtc@aggTrade';

        // Socket 的建立
        $loop = ReactFactory::create();
        $reactConnector = new SocketConnector($loop, [
            'tls' => [
                'verify_peer' => false,
                'verify_peer_name' => false
            ],
        ]);

        // Socket 的連線
        $connector = new ClientConnector($loop, $reactConnector);
        $connector($uri)->then(function(WebSocket $conn) {
            $conn->on('message', function(MessageInterface $msg) use ($conn) {
                echo 'Received: '. $msg . PHP_EOL;
            });

            $conn->on('close', function($code = null, $reason = null) {
                echo 'Code: ' . $code . PHP_EOL . 'Reason: ' . $reason . PHP_EOL . 'Connection closed';
            });
        }, function(Throwable $e) use ($loop) {
            echo 'Could not connect: '. $e->getMessage() . PHP_EOL;
            $loop->stop();
        });
        $loop->run();
    }
}