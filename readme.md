# Lumen PHP Framework

[![Build Status](https://travis-ci.org/laravel/lumen-framework.svg)](https://travis-ci.org/laravel/lumen-framework)
[![Total Downloads](https://poser.pugx.org/laravel/lumen-framework/d/total.svg)](https://packagist.org/packages/laravel/lumen-framework)
[![Latest Stable Version](https://poser.pugx.org/laravel/lumen-framework/v/stable.svg)](https://packagist.org/packages/laravel/lumen-framework)
[![Latest Unstable Version](https://poser.pugx.org/laravel/lumen-framework/v/unstable.svg)](https://packagist.org/packages/laravel/lumen-framework)
[![License](https://poser.pugx.org/laravel/lumen-framework/license.svg)](https://packagist.org/packages/laravel/lumen-framework)

Laravel Lumen is a stunningly fast PHP micro-framework for building web applications with expressive, elegant syntax. We believe development must be an enjoyable, creative experience to be truly fulfilling. Lumen attempts to take the pain out of development by easing common tasks used in the majority of web projects, such as routing, database abstraction, queueing, and caching.

## Official Documentation

Documentation for the framework can be found on the [Lumen website](http://lumen.laravel.com/docs).

## Security Vulnerabilities

If you discover a security vulnerability within Lumen, please send an e-mail to Taylor Otwell at taylor@laravel.com. All security vulnerabilities will be promptly addressed.

## Demo Web Socket Client
 
 新增一個指令 demo client 端實際接收到的資料
 
 ```
 → php artisan demo:client
 Received: {"e":"aggTrade","E":1520927004725,"s":"BNBBTC","a":9908245,"p":"0.00087860","q":"400.75000000","f":11373546,"l":11373546,"T":1520927004721,"m":true,"M":true}
 Received: {"e":"aggTrade","E":1520927005702,"s":"BNBBTC","a":9908246,"p":"0.00087900","q":"0.30000000","f":11373547,"l":11373547,"T":1520927005697,"m":false,"M":true}
 Received: {"e":"aggTrade","E":1520927007930,"s":"BNBBTC","a":9908247,"p":"0.00087850","q":"42.26000000","f":11373548,"l":11373548,"T":1520927007927,"m":true,"M":true}
 Received: {"e":"aggTrade","E":1520927013466,"s":"BNBBTC","a":9908248,"p":"0.00087900","q":"11.37000000","f":11373549,"l":11373549,"T":1520927013462,"m":false,"M":true}
 Received: {"e":"aggTrade","E":1520927017339,"s":"BNBBTC","a":9908249,"p":"0.00087900","q":"10.60000000","f":11373550,"l":11373550,"T":1520927017335,"m":false,"M":true}
 Received: {"e":"aggTrade","E":1520927017410,"s":"BNBBTC","a":9908250,"p":"0.00087900","q":"12.08000000","f":11373551,"l":11373551,"T":1520927017407,"m":false,"M":true}
 Received: {"e":"aggTrade","E":1520927019124,"s":"BNBBTC","a":9908251,"p":"0.00087880","q":"900.46000000","f":11373552,"l":11373552,"T":1520927019122,"m":false,"M":true}
 Received: {"e":"aggTrade","E":1520927019144,"s":"BNBBTC","a":9908252,"p":"0.00087860","q":"625.33000000","f":11373553,"l":11373553,"T":1520927019141,"m":true,"M":true}
 Received: {"e":"aggTrade","E":1520927026047,"s":"BNBBTC","a":9908253,"p":"0.00087900","q":"0.03000000","f":11373554,"l":11373554,"T":1520927026044,"m":false,"M":true}
 ...
 ```

## License

The Lumen framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)
